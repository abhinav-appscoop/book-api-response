import 'package:flutter/foundation.dart';

//class Post {
//  final int userId;
//  final int id;
//  final String title;
//  final String body;
//
//  Post({
//    @required this.userId,
//    @required this.id,
//    @required this.title,
//    @required this.body,
//  });
//
//  factory Post.fromJson(Map<String, dynamic> json) {
//    return Post(
//      userId: json['userId'] as int,
//      id: json['id'] as int,
//      title: json['title'] as String,
//      body: json['body'] as String,
//    );
//  }
//}

class Post {
  String kind;
  int totalItems;
  List<Item> items;

  Post({
    @required this.kind,
    @required this.totalItems,
    @required this.items,
  });

  factory Post.fromJson(Map<String, dynamic> json) => Post(
    kind: json["kind"],
    totalItems: json["totalItems"],
    items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "kind": kind,
    "totalItems": totalItems,
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
  };
}
class Item {
  Kind kind;
  String id;
  String etag;
  String selfLink;
  VolumeInfo volumeInfo;
  SaleInfo saleInfo;
  AccessInfo accessInfo;
  SearchInfo searchInfo;

  Item({
    @required this.kind,
    @required this.id,
    @required this.etag,
    @required this.selfLink,
    @required this.volumeInfo,
    @required this.saleInfo,
    @required this.accessInfo,
    @required this.searchInfo,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    kind: kindValues.map[json["kind"]],
    id: json["id"],
    etag: json["etag"],
    selfLink: json["selfLink"],
    volumeInfo: VolumeInfo.fromJson(json["volumeInfo"]),
    saleInfo: SaleInfo.fromJson(json["saleInfo"]),
    accessInfo: AccessInfo.fromJson(json["accessInfo"]),
    searchInfo: json["searchInfo"] == null ? null : SearchInfo.fromJson(json["searchInfo"]),
  );

  Map<String, dynamic> toJson() => {
    "kind": kindValues.reverse[kind],
    "id": id,
    "etag": etag,
    "selfLink": selfLink,
    "volumeInfo": volumeInfo.toJson(),
    "saleInfo": saleInfo.toJson(),
    "accessInfo": accessInfo.toJson(),
    "searchInfo": searchInfo == null ? null : searchInfo.toJson(),
  };
}

class AccessInfo {
  Country country;
  Viewability viewability;
  bool embeddable;
  bool publicDomain;
  TextToSpeechPermission textToSpeechPermission;
  Epub epub;
  Epub pdf;
  String webReaderLink;
  AccessViewStatus accessViewStatus;
  bool quoteSharingAllowed;

  AccessInfo({
    @required this.country,
    @required this.viewability,
    @required this.embeddable,
    @required this.publicDomain,
    @required this.textToSpeechPermission,
    @required this.epub,
    @required this.pdf,
    @required this.webReaderLink,
    @required this.accessViewStatus,
    @required this.quoteSharingAllowed,
  });

  factory AccessInfo.fromJson(Map<String, dynamic> json) => AccessInfo(
    country: countryValues.map[json["country"]],
    viewability: viewabilityValues.map[json["viewability"]],
    embeddable: json["embeddable"],
    publicDomain: json["publicDomain"],
    textToSpeechPermission: textToSpeechPermissionValues.map[json["textToSpeechPermission"]],
    epub: Epub.fromJson(json["epub"]),
    pdf: Epub.fromJson(json["pdf"]),
    webReaderLink: json["webReaderLink"],
    accessViewStatus: accessViewStatusValues.map[json["accessViewStatus"]],
    quoteSharingAllowed: json["quoteSharingAllowed"],
  );

  Map<String, dynamic> toJson() => {
    "country": countryValues.reverse[country],
    "viewability": viewabilityValues.reverse[viewability],
    "embeddable": embeddable,
    "publicDomain": publicDomain,
    "textToSpeechPermission": textToSpeechPermissionValues.reverse[textToSpeechPermission],
    "epub": epub.toJson(),
    "pdf": pdf.toJson(),
    "webReaderLink": webReaderLink,
    "accessViewStatus": accessViewStatusValues.reverse[accessViewStatus],
    "quoteSharingAllowed": quoteSharingAllowed,
  };
}
enum AccessViewStatus { FULL_PUBLIC_DOMAIN }

final accessViewStatusValues = EnumValues({
  "FULL_PUBLIC_DOMAIN": AccessViewStatus.FULL_PUBLIC_DOMAIN
});

enum Country { IN }

final countryValues = EnumValues({
  "IN": Country.IN
});

class Epub {
  bool isAvailable;
  String downloadLink;

  Epub({
    @required this.isAvailable,
    @required this.downloadLink,
  });

  factory Epub.fromJson(Map<String, dynamic> json) => Epub(
    isAvailable: json["isAvailable"],
    downloadLink: json["downloadLink"] == null ? null : json["downloadLink"],
  );

  Map<String, dynamic> toJson() => {
    "isAvailable": isAvailable,
    "downloadLink": downloadLink == null ? null : downloadLink,
  };
}

enum TextToSpeechPermission { ALLOWED }

final textToSpeechPermissionValues = EnumValues({
  "ALLOWED": TextToSpeechPermission.ALLOWED
});

enum Viewability { ALL_PAGES }

final viewabilityValues = EnumValues({
  "ALL_PAGES": Viewability.ALL_PAGES
});

enum Kind { BOOKS_VOLUME }

final kindValues = EnumValues({
  "books#volume": Kind.BOOKS_VOLUME
});

class SaleInfo {
  Country country;
  Saleability saleability;
  bool isEbook;
  String buyLink;

  SaleInfo({
    @required this.country,
    @required this.saleability,
    @required this.isEbook,
    @required this.buyLink,
  });

  factory SaleInfo.fromJson(Map<String, dynamic> json) => SaleInfo(
    country: countryValues.map[json["country"]],
    saleability: saleabilityValues.map[json["saleability"]],
    isEbook: json["isEbook"],
    buyLink: json["buyLink"],
  );

  Map<String, dynamic> toJson() => {
    "country": countryValues.reverse[country],
    "saleability": saleabilityValues.reverse[saleability],
    "isEbook": isEbook,
    "buyLink": buyLink,
  };
}

enum Saleability { FREE }

final saleabilityValues = EnumValues({
  "FREE": Saleability.FREE
});

class SearchInfo {
  String textSnippet;

  SearchInfo({
    @required this.textSnippet,
  });

  factory SearchInfo.fromJson(Map<String, dynamic> json) => SearchInfo(
    textSnippet: json["textSnippet"],
  );

  Map<String, dynamic> toJson() => {
    "textSnippet": textSnippet,
  };
}

class VolumeInfo {
  String title;
  String subtitle;
  String publishedDate;
  List<IndustryIdentifier> industryIdentifiers;
  ReadingModes readingModes;
  PrintType printType;
  List<String> categories;
  MaturityRating maturityRating;
  bool allowAnonLogging;
  String contentVersion;
  PanelizationSummary panelizationSummary;
  ImageLinks imageLinks;
  Language language;
  String previewLink;
  String infoLink;
  String canonicalVolumeLink;
  List<String> authors;
  String description;
  int pageCount;
  double averageRating;
  int ratingsCount;
  String publisher;

  VolumeInfo({
    @required this.title,
    @required this.subtitle,
    @required this.publishedDate,
    @required this.industryIdentifiers,
    @required this.readingModes,
    @required this.printType,
    @required this.categories,
    @required this.maturityRating,
    @required this.allowAnonLogging,
    @required this.contentVersion,
    @required this.panelizationSummary,
    @required this.imageLinks,
    @required this.language,
    @required this.previewLink,
    @required this.infoLink,
    @required this.canonicalVolumeLink,
    @required this.authors,
    @required this.description,
    @required this.pageCount,
    @required this.averageRating,
    @required this.ratingsCount,
    @required this.publisher,
  });

  factory VolumeInfo.fromJson(Map<String, dynamic> json) => VolumeInfo(
    title: json["title"],
    subtitle: json["subtitle"] == null ? null : json["subtitle"],
    publishedDate: json["publishedDate"],
    industryIdentifiers: List<IndustryIdentifier>.from(json["industryIdentifiers"].map((x) => IndustryIdentifier.fromJson(x))),
    readingModes: ReadingModes.fromJson(json["readingModes"]),
    printType: printTypeValues.map[json["printType"]],
    categories: json["categories"] == null ? null : List<String>.from(json["categories"].map((x) => x)),
    maturityRating: maturityRatingValues.map[json["maturityRating"]],
    allowAnonLogging: json["allowAnonLogging"],
    contentVersion: json["contentVersion"],
    panelizationSummary: json["panelizationSummary"] == null ? null : PanelizationSummary.fromJson(json["panelizationSummary"]),
    imageLinks: ImageLinks.fromJson(json["imageLinks"]),
    language: languageValues.map[json["language"]],
    previewLink: json["previewLink"],
    infoLink: json["infoLink"],
    canonicalVolumeLink: json["canonicalVolumeLink"],
    authors: json["authors"] == null ? null : List<String>.from(json["authors"].map((x) => x)),
    description: json["description"] == null ? null : json["description"],
    pageCount: json["pageCount"] == null ? null : json["pageCount"],
    averageRating: json["averageRating"] == null ? null : json["averageRating"].toDouble(),
    ratingsCount: json["ratingsCount"] == null ? null : json["ratingsCount"],
    publisher: json["publisher"] == null ? null : json["publisher"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "subtitle": subtitle == null ? null : subtitle,
    "publishedDate": publishedDate,
    "industryIdentifiers": List<dynamic>.from(industryIdentifiers.map((x) => x.toJson())),
    "readingModes": readingModes.toJson(),
    "printType": printTypeValues.reverse[printType],
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x)),
    "maturityRating": maturityRatingValues.reverse[maturityRating],
    "allowAnonLogging": allowAnonLogging,
    "contentVersion": contentVersion,
    "panelizationSummary": panelizationSummary == null ? null : panelizationSummary.toJson(),
    "imageLinks": imageLinks.toJson(),
    "language": languageValues.reverse[language],
    "previewLink": previewLink,
    "infoLink": infoLink,
    "canonicalVolumeLink": canonicalVolumeLink,
    "authors": authors == null ? null : List<dynamic>.from(authors.map((x) => x)),
    "description": description == null ? null : description,
    "pageCount": pageCount == null ? null : pageCount,
    "averageRating": averageRating == null ? null : averageRating,
    "ratingsCount": ratingsCount == null ? null : ratingsCount,
    "publisher": publisher == null ? null : publisher,
  };
}

class ImageLinks {
  String smallThumbnail;
  String thumbnail;

  ImageLinks({
    @required this.smallThumbnail,
    @required this.thumbnail,
  });

  factory ImageLinks.fromJson(Map<String, dynamic> json) => ImageLinks(
    smallThumbnail: json["smallThumbnail"],
    thumbnail: json["thumbnail"],
  );

  Map<String, dynamic> toJson() => {
    "smallThumbnail": smallThumbnail,
    "thumbnail": thumbnail,
  };
}

class IndustryIdentifier {
  Type type;
  String identifier;

  IndustryIdentifier({
    @required this.type,
    @required this.identifier,
  });

  factory IndustryIdentifier.fromJson(Map<String, dynamic> json) => IndustryIdentifier(
    type: typeValues.map[json["type"]],
    identifier: json["identifier"],
  );

  Map<String, dynamic> toJson() => {
    "type": typeValues.reverse[type],
    "identifier": identifier,
  };
}

enum Type { OTHER }

final typeValues = EnumValues({
  "OTHER": Type.OTHER
});

enum Language { EN }

final languageValues = EnumValues({
  "en": Language.EN
});

enum MaturityRating { NOT_MATURE }

final maturityRatingValues = EnumValues({
  "NOT_MATURE": MaturityRating.NOT_MATURE
});

class PanelizationSummary {
  bool containsEpubBubbles;
  bool containsImageBubbles;

  PanelizationSummary({
    @required this.containsEpubBubbles,
    @required this.containsImageBubbles,
  });

  factory PanelizationSummary.fromJson(Map<String, dynamic> json) => PanelizationSummary(
    containsEpubBubbles: json["containsEpubBubbles"],
    containsImageBubbles: json["containsImageBubbles"],
  );

  Map<String, dynamic> toJson() => {
    "containsEpubBubbles": containsEpubBubbles,
    "containsImageBubbles": containsImageBubbles,
  };
}

enum PrintType { BOOK }

final printTypeValues = EnumValues({
  "BOOK": PrintType.BOOK
});

class ReadingModes {
  bool text;
  bool image;

  ReadingModes({
    @required this.text,
    @required this.image,
  });

  factory ReadingModes.fromJson(Map<String, dynamic> json) => ReadingModes(
    text: json["text"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "text": text,
    "image": image,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

