import 'dart:convert';

import 'package:flutter_bookapi/model/post_model.dart';
import 'package:http/http.dart' as http;

class HttpService {
  final String postsURL = "https://www.googleapis.com/books/v1/volumes?filter=free-ebooks&q=a";

  Future<Post> fetchAlbum() async {
    final response = await http.get(postsURL);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Post.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}