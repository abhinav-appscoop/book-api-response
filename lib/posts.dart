
import 'package:flutter/material.dart';
import 'package:flutter_bookapi/model/post_model.dart';
import 'package:flutter_bookapi/post_detail.dart';
import 'package:flutter_bookapi/service/http_service.dart';

import 'model/bookdetail_response.dart';


class PostsPage extends StatefulWidget {
  PostsPage({Key key}) : super(key: key);

  @override
  PostsPageState createState() => PostsPageState();
}

class PostsPageState extends State<PostsPage> {
  Future<Post> futureAlbum;
  final HttpService httpService = HttpService();

  @override
  void initState() {
    super.initState();
    futureAlbum = httpService.fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<Post>(
            future: futureAlbum,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return  new ListView.builder(
                    itemCount: snapshot.data.items.length == null ? 0 : snapshot.data.items.length,
                    itemBuilder: (BuildContext context,int index){
                      return new Container(

                        child: new Center(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              new Card(
                                child :InkWell(
                                  child: new Container(
                                    child: new Text(snapshot.data.items[index].volumeInfo.title),
                                    padding: const EdgeInsets.all(20.0),
                                  ),
                                  onTap: () {
                                    var  detail = snapshot.data.items[index].volumeInfo.subtitle;
                                    var  image = snapshot.data.items[index].volumeInfo.imageLinks.thumbnail;
                                    print(detail);
                                    print(image);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BookDetial(
                                            pass:Book(
                                              description: detail,
                                              imagePath: image,
                                            ),
                                          )),
                                    );
                                  },

                                ),

                              ),
                            ],
                          ),
                        ),
                      );
                    }
                );

              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),

      ),
    );
  }
}